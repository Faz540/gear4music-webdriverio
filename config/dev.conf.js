const configTemplate = require("./template/wdio.conf").config;

const devConfig = {
    ...configTemplate,
    baseUrl: "http://www.dev.gear4music.com"
}

exports.config = devConfig;