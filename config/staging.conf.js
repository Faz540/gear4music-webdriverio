const configTemplate = require("./template/wdio.conf").config;

const stagingConfig = {
    ...configTemplate,
    baseUrl: "http://www.stg.gear4music.com"
}

exports.config = stagingConfig;