const configTemplate = require("./template/wdio.conf").config;

const productionConfig = {
    ...configTemplate,
    baseUrl: "http://www.gear4music.com"
}

exports.config = productionConfig;