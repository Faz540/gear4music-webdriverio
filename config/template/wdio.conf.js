const fs = require("fs-extra");
const actions = require("../../helpers/actions");

exports.config = {
    runner: 'local',
    path: '/',
    specs: [
        './tests/*.spec.js'
    ],
    maxInstances: 5,
    capabilities: [{
        browserName: 'chrome',
        // Uncomment the below to run Headless Chrome:
        "goog:chromeOptions": {
        "args": ["--headless", "--disable-gpu", "--window-size=1920,1080"]
        }
    }],
    // Level of logging verbosity: trace | debug | info | warn | error | silent
    logLevel: 'error',
    // Default timeout for all waitFor* commands.
    waitforTimeout: 10000,
    // Default request retries count
    connectionRetryCount: 3,
    services: ['chromedriver'],
    framework: 'mocha',
    reporters: ['spec'],
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
    // Hooks:
    onPrepare: function() {
        // Removes screenshots from the "screenshots" folder before the tests are ran.
        // This is so there are no screenshots present from previous test runs.
        actions.removeAllFilesFromDirectory('screenshots');
    },
    afterTest: function(test) {
        // If a test fails or errors, take a screenshot.
        // Test Failure = Chai assertion failure
        // Test Error = Trying to interact with an element that doesn't exist etc.
        if(test.passed === false || test.error) {
            if(!fs.existsSync(`./screenshots/${test.parent}`)) {
                fs.mkdirSync(`./screenshots/${test.parent}`);
            }
            const filePath = `./screenshots/${test.parent}/${test.title}.png`;
            browser.saveScreenshot(filePath);
        }
    }
}
