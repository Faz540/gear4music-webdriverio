// Assertion Library Dependencies:
const chai = require('chai');
const expect = require("chai").expect;
const chaiWebdriver = require('chai-webdriverio').default;
chai.use(chaiWebdriver(browser));

// Helpers:
const actions = require("../helpers/actions");

// Pages:
const homePage = require("../pages/home.page");
const plp = require("../pages/plp.page");

describe("Smoke Tests", function() {
    before(function() {
        homePage.open();
        browser.maximizeWindow();
        actions.closeCookieBanner();
    });
    describe("PLP", function() {
        before(function() {
            plp.open("/Fender.html");
        });
        it("expects at least one product card to be displayed", function() {
            expect(plp.numberOfResults()).to.be.at.least(1);
        });
        it("expects the 'Sort By' option to be displayed", function() {
            expect(plp.dropdownSortBy.selector).to.be.displayed();
        });
        it("expects the 'Availability' option to be displayed", function() {
            expect(plp.dropdownAvailability.selector).to.be.displayed();
        });
        it("expects the 'Price Range' option to be displayed", function() {
            expect(plp.dropdownPriceRange.selector).to.be.displayed();
        });
        it("expects at least four filters to be displayed", function() {
            // The first 3 filters are ["Sort By", "Availability", "Price Range"]
            // The above 3 filters are always shown on PLPs (Unless they're Curated PLPs)
            expect(plp.numberOfFilters()).to.be.at.least(4);
        });
        it("expects the 'Colour' filter to correctly filter the products", function() {
            const maxNumberOfAvailableProductsBeforeFiltering = plp.maxNumberOfAvailableProducts();
            plp.filterBy("Colour", "Purple");
            const maxNumberOfAvailableProductsAfterFiltering = plp.maxNumberOfAvailableProducts();
            expect(maxNumberOfAvailableProductsAfterFiltering).to.be.lessThan(maxNumberOfAvailableProductsBeforeFiltering);
        });
    });
});