// Assertion Library Dependencies:
const chai = require('chai');
const expect = require("chai").expect;
const chaiWebdriver = require('chai-webdriverio').default;
chai.use(chaiWebdriver(browser));

// Helpers:
const actions = require("../helpers/actions");

// Pages:
const homePage = require("../pages/home.page");
const header = require("../pages/header.menu");
const searchResultsPage = require("../pages/searchResults.page");

describe("Smoke Tests", function() {
    before(function() {
        homePage.open();
        browser.maximizeWindow();
        actions.closeCookieBanner();
    });
    describe("Search", function() {
        it("successfully performs a search and results are returned", function() {
            header.searchFor("Red");
            expect(searchResultsPage.numberOfResults()).to.be.at.least(1);
        });
    });
});