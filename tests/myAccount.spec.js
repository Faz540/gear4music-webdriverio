// Assertion Library Dependencies:
const chai = require('chai');
const expect = require("chai").expect;
const chaiWebdriver = require('chai-webdriverio').default;
chai.use(chaiWebdriver(browser));

// Helpers:
const actions = require("../helpers/actions");
const waitUntil = require("../helpers/waits");

// Pages:
const homePage = require("../pages/home.page");
const myAccountPage = require("../pages/myAccount.page");

describe("Smoke Tests", function() {
    before(function() {
        homePage.open();
        browser.maximizeWindow();
        actions.closeCookieBanner();
    });
    describe("My Account", function() {
        after(function() {
            actions.logout();
        });
        it("successfully logs in with valid credentials", function() {
            myAccountPage.open();
            myAccountPage.login("test-user.united-kingdom@g4m.co.uk", "Password1");
            waitUntil.elementNoLongerExists(myAccountPage.formLogin);
            const currentUrl = browser.getUrl();
            expect(currentUrl).to.contain("/my-account");
            expect(myAccountPage.buttonLogOut.selector).to.be.displayed();
        });
    });
});