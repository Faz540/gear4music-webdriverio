// Assertion Library Dependencies:
const chai = require('chai');
const expect = require("chai").expect;
const chaiWebdriver = require('chai-webdriverio').default;
chai.use(chaiWebdriver(browser));

// Helpers:
const actions = require("../helpers/actions");

// Pages:
const homePage = require("../pages/home.page");
const header = require("../pages/header.menu");
const pdp = require("../pages/pdp.page");
const basketPage = require("../pages/basket.page");

describe("Smoke Tests", function() {
    before(function() {
        homePage.open();
        browser.maximizeWindow();
        actions.closeCookieBanner();
    });
    describe("PDP", function() {
        before(function() {
            pdp.open("/g4m/5");
        });
        it("successfully adds the product to the User's basket", function() {
            pdp.addToBasket();
            expect(header.textBasketItems.selector).to.have.text("1");
            expect(basketPage.basketItems.selector).to.have.count(1);
            
        });
    });
});