const fs = require("fs-extra");
const waitUntil = require("./waits");
const homePage = require("../pages/home.page");

class Actions {
    removeAllFilesFromDirectory(directoryName) {
        const directory = fs.readdirSync(`./${directoryName}`);
        const screenshots = directory.filter(function(file) {
            return !file.includes(".ignoreMe");
        })
        return screenshots.forEach(function(screenshot) {
            return fs.removeSync(`./${directoryName}/${screenshot}`);
        });
    };
    
    closeCookieBanner() {
        const cookieBanner = $(".cookie-consent-content");
        if(cookieBanner.isExisting()) {
            $(".cookie-consent-content button").click(); 
        };
        waitUntil.elementNoLongerExists(cookieBanner);
    };

    logout() {
        browser.url("/auth/logout");
        return waitUntil.elementExists(homePage.pageContent);
    };
};

module.exports = new Actions;