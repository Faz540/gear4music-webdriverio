class WaitUntil {
    elementNoLongerExists(element) {
        return $(element.selector).waitForExist(10000, true, `Element "${element.selector}" still exists after waiting 10 seconds.`);
    };

    elementExists(element) {
        return $(element.selector).waitForExist(10000, false, `Element "${element.selector}" didn't exist after waiting 10 seconds.`);
    };
};

module.exports = new WaitUntil;