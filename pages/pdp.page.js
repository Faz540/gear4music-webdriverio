const waitUntil = require("../helpers/waits");
const basketPage = require("./basket.page");

class ProductDetailPage {
    // Elements:
    get pageContent() { return $(".pdp-page"); }
    get buttonAddToBasket() { return $("a.add-to-basket-button"); }

    // Page Functions:
    open(urlPath) {
        browser.url(urlPath);
        return waitUntil.elementExists(this.pageContent);
    };

    addToBasket() {
        this.buttonAddToBasket.click();
        return waitUntil.elementExists(basketPage.formBasket);
    };
};

module.exports = new ProductDetailPage;