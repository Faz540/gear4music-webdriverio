class MyAccountPage {
    // Elements:
    get inputEmail() { return $("#login_form #str_email") ;}
    get inputPassword() { return $("#login_form #str_password") ;}
    get buttonLogin() { return $("#login_form [type='submit']") ;}
    get buttonLogOut() { return $(".button[href='/auth/logout']"); }
    get formLogin() { return $("#login_form"); }

    // Page Functions:
    open() {
        return browser.url("/auth/login");
    };

    login(email, password) {
        this.inputEmail.clearValue();
        this.inputEmail.setValue(email);
        this.inputPassword.clearValue();
        this.inputPassword.setValue(password);
        return this.buttonLogin.click();
    };
};

module.exports = new MyAccountPage;