const waitUntil = require("../helpers/waits");

class ProductListingPage {
    // Elements:
    get pageContent() { return $("[data-test='plp-product-cards']"); }
    get overlayLoadingResults() { return $(".g4m-loading-overlay"); }
    get productCards() { return $$("[data-test='desktop/plp/product-card']"); }
    get dropdownSortBy() { return $("[data-test='desktop/plp/open-dropdown-sort']"); }
    get dropdownAvailability() { return $("[data-test='desktop/plp/open-dropdown-availability']"); }
    get dropdownPriceRange() { return $("[data-test='desktop/plp/open-dropdown-price']"); }
    get dropdownFilters() { return $$("[data-test*='desktop/plp/open-dropdown-']"); }
    get buttonShowAllFilters() { return $("[data-test='desktop/plp/filters/view-all-button']"); }
    get buttonShowLessFilters() { return $("[data-test='desktop/plp/filters/view-less-button]"); }
    get listFilterOptions() { return $$("[data-test*='desktop/plp/dropdown-options-list'] button")}
    get textShowingXOfYProducts() { return $$("[data-test='plp-product-listing-count-message'] span"); }
    
    // Page Functions:
    open(urlPath) {
        browser.url(urlPath);
        return waitUntil.elementExists(this.pageContent);
    };
    
    numberOfResults() {
        return this.productCards.length;
    };
    
    numberOfFilters() {
        return this.dropdownFilters.length;
    };
    
    maxNumberOfAvailableProducts() {
        return parseInt(this.textShowingXOfYProducts[1].getText());
    };
    
    showAllFilters() {
        // Show all filters as they can be hidden dependent on screen size
        this.buttonShowAllFilters.click(); 
    };
    
    openFilter(filterName) {
        const filters = this.dropdownFilters;
        const desiredFilter = filters.find(function(filter) {
            return browser.getElementText(filter.elementId).includes(filterName);
        });
        return desiredFilter.click();      
    };
    
    selectFilterOption(filterOptionName) {
        const filterOptions = this.listFilterOptions;
        const desiredFilterOption = filterOptions.find(function(filterOption) {
            return browser.getElementText(filterOption.elementId).includes(filterOptionName);
        });
        return desiredFilterOption.click();   
    };
    
    filterBy(filterName, filterOption) {
        this.openFilter(filterName);
        this.selectFilterOption(filterOption);
        return waitUntil.elementNoLongerExists(this.overlayLoadingResults);
    };
};

module.exports = new ProductListingPage;