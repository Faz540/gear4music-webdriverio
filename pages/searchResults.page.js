class SearchResultsPage {
    // Elements:
    get linkSearchResults() { return $$(".list-row-container"); }
    get carousel() { return $(".carousel"); }
    
    // Page Functions:
    numberOfResults() {
        return this.linkSearchResults.length;
    }
};

module.exports = new SearchResultsPage;