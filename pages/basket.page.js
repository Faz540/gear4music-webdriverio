class BasketPage {
    // Elements:
    get formBasket() { return $("#basket-form"); }
    get basketItems() { return $$(".product-listing"); }

    // Page Functions:
    numberOfBasketItems() {
        return this.basketItems.length;
    };
};

module.exports = new BasketPage;