class HomePage {
    // Elements:
    get pageContent() { return $("[data-test='home-content-desktop']"); }
    
    // Page Functions:
    open() {
        return browser.url("/");
    };
};

module.exports = new HomePage;