const waitUntil = require("../helpers/waits");
const searchResults = require("./searchResults.page");

class Header {
    // Elements:
    get inputSearch() { return $("#srch-str"); }
    get buttonSearch() { return $("#header-search-button"); }
    get textBasketItems() { return $("#basket-item-count"); }
    
    // Page Functions:
    searchFor(searchTerm) {
        this.inputSearch.clearValue();
        this.inputSearch.setValue(searchTerm);
        this.buttonSearch.click();
        return waitUntil.elementNoLongerExists(searchResults.carousel);
    };

    numberOfBasketItems() {
        return parseInt(this.textBasketItems.getText());
    };
};

module.exports = new Header;